# About

`mkpseudo` is a program written in Python aimed at converting existing code into pseudocode using markdown.

This project is currently [being rewritten](https://gitlab.com/Iron_E/mkpseudo-dart) in Dart.

# Usage

I run `mkpseudo` from a .sh file, but it isn't necessary. Here's the .sh file:

```bash
python ~/$PATH_TO_MKPSEUDO/mkpseudo/ModuleSelector.py "$1" "$2"
```

This command can be run directly in the command line, but the .sh file is easier. 

`$1` is the sourcefile (e.g. "foo.java", "bar.cs"), and `$2` is the destination file for the translation (e.g. "pseudocode.md").

## Project-Wide 

If you want to load a whole project into `mkpseudo`, you could do something like this:

```bash
for f in $(cat "$1") ; do
        mkpseudo.sh "$f" "$2"
done
```

And call it like so: `mkpseudo.sh $(find /$PROJECT_FOLDER/src/ -name "*.java") pseudocode.md`

# Language Support

As I find time, I may add more language support. These are languages I am currently interested in supporting:

* [ ] C 
* [ ] C#
* [X] Java
* [ ] Python
