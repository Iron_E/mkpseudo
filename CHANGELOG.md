# v1.X

## v1.0

* Add support for Java translation

## v1.1

* __Feature__: Collapse indices (sorry, no generics)
* __Feature__: Update README.md with bash script to translate list of files
* __Bug Fix__: Keywords are correctly detected with refactored algorithms
* __Misc__: The program no longer detects `enum`s and `interface`s.
	* These types do not need to be translated into pseudocode 
