import sys

from dotJava.files.inp.JavaFileReader import JavaFileReader

"""
==MODULE STATISTICS==
Java → Pseudocode: ~1,759.615 lines:sec
"""

if __name__ == '__main__':
	if len(sys.argv) < 3:
		print("You must specify a source file, and a target *.md file.")
		sys.exit()
	else:
		# This variable isn't just a one-off, it will be used more as the program grows more complex
		extension = sys.argv[1].split('.')[1]

		if extension == 'java':
			# Remove this line after program is complete
			# with open(sys.argv[2], 'w') as mdFile:
			#	pass
			# ^ that clears the file; this is useful for testing

			JavaFileReader.read_passed_file()
		else:
			print("The programming language entered is not currently supported.")
