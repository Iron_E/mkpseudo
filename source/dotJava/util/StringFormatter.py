from ..LangLibrary import ConversionTables, Keywords
from .StringManipulator import StringManipulator


class StringFormatter:
	@staticmethod
	def class_to_type(className):
		# Convert generics to have spaces around them
		className = str(
				StringManipulator.exec_conversion_table(
						ConversionTables.class_header_generics,
						className, True
				)
		).split()

		return "`" + className[0] + "`" + StringFormatter.__class_generics_to_type(
				className[1:])

	@staticmethod
	def class_to_returning_type(listToWrite):
		for j in range(len(listToWrite)):
			listToWrite[j] = listToWrite[j].replace(",", ", ")
		# print(listToWrite)
		objectWriteout = ", "
		if ('<' in listToWrite[0]) and (">" not in listToWrite[0]):
			typeToWrite = ""
			nameToWrite = ""

			openBracketCount = 0
			closeBracketCount = 0

			for k in range(len(listToWrite)):
				if '<' in listToWrite[k]:
					openBracketCount += 1
				if '>' in listToWrite[k]:
					closeBracketCount += 1

				if openBracketCount == closeBracketCount:
					nameToWrite = listToWrite[k + 1]
					break

				typeToWrite += listToWrite[k]

		else:
			typeToWrite = listToWrite[0]
			try:
				nameToWrite = listToWrite[1]
			except IndexError:
				nameToWrite = ""
		if StringManipulator.list_element_in_string(
				typeToWrite, Keywords.types_generic):
			objectWriteout += "an "
			if StringManipulator.list_element_in_string(
					typeToWrite, Keywords.types_generic_array):
				objectWriteout += "array"
			else:
				objectWriteout += "object"
			objectWriteout += " of generic type "
		else:
			objectWriteout += "a(n) "

		return str(StringManipulator.capitalize_first_character(
				nameToWrite) + objectWriteout + StringFormatter.class_to_type(typeToWrite))

	@staticmethod
	def __class_generics_to_type(remainingList):
		def is_extends(nextWord):
			return str(nextWord).lower() == 'extends'

		# Set to length of list so it can be reduced later
		genericsInHeader = len(remainingList)

		genericsWriteout = ""

		# Get count of
		for word in remainingList:
			if (word in Keywords.object_generics_indices) or (word == ','):
				genericsInHeader -= 1
			if is_extends(word):
				genericsInHeader -= 2

		# Detect if there are any generics at all
		if all(indice in remainingList for indice in Keywords.object_generics_indices):

			# If so, initialize the output
			genericsWriteout += " with internal generic type"
			if "," in remainingList:
				# If there are multiple generics, correct the grammar
				genericsWriteout += "s"

			# Add remaining variable for grammar checking
			genericsInHeaderRemaining = genericsInHeader

			# Iterator over the list passed in
			for i in range(len(remainingList)):
				# Ensure it is not a keyword or a chevron
				if (remainingList[i] not in ['extends', ',', "´s"]) and (remainingList[i] not in
				                                                   Keywords.object_generics_indices):
					# Separate wildcard
					if remainingList[i] != '?':
						# Put the variable character on the line
						genericsWriteout += " `" + remainingList[i]
						try:
							if is_extends(remainingList[i + 1]):
								# If "extends" follows the current word, add more information
								genericsWriteout += "`, a subtype of `" + remainingList[i + 2] + "`"
								i += 2
						except IndexError:
							pass
						genericsWriteout += '`'

					else:
						# Account for the wildcard
						genericsWriteout = " with an unspecified internal generic type"
						try:
							if is_extends(remainingList[i + 1]):
								# If "extends" comes next, write something that makes grammatical sense
								genericsWriteout += " that extends `" + remainingList[i + 2]
								i += 2
						except IndexError:
							pass

					genericsInHeaderRemaining -= 1

				elif remainingList[i] not in Keywords.object_generics_indices:
					if remainingList[i] == ",":
						if (genericsInHeader > 2) and (genericsInHeaderRemaining > 1):
							genericsWriteout += ", "
						else:
							genericsWriteout += " and "
					elif remainingList[i] == "´s":
						genericsWriteout += "´s"

		return genericsWriteout

	@staticmethod
	def format_to_pseudocode(toFormat):
		# Run the conversion tables
		toFormat = StringManipulator.remove_words(
				StringManipulator.exec_conversion_table(
						ConversionTables.keywords, StringManipulator.exec_conversion_table(
								ConversionTables.symbols, toFormat).replace('.', "´s ")
				), Keywords.modifiers
		)

		return toFormat.strip()

	@staticmethod
	def format_to_tabbed_list_entry(toFormat):
		return f"\n\t* {toFormat}"

	@staticmethod
	def insert_spaces_in_exception_title(title):
		listCopy = list(title)

		replacementIndexes = []

		for i in range(len(listCopy)):
			if listCopy[i].isupper():
				replacementIndexes.append(i)

		for i in range(len(replacementIndexes)):
			listCopy.insert(replacementIndexes[i] + i, " ")

		return ''.join(listCopy).strip()

	@staticmethod
	def prepare_method_body(methodBody):
		for i in range(len(methodBody)):
			methodBody[i] = StringManipulator.exec_conversion_table(
					ConversionTables.post_corrections,
					StringManipulator.exec_conversion_table(
						ConversionTables.keywords,
						StringManipulator.exec_conversion_table(
								ConversionTables.syntax_splitter_compatible,
								methodBody[i], True
						)
					), True
			)

			splitter = methodBody[i].split()
			for j in range(len(splitter)):
				try:
					if splitter[j][-2] == "´":
						splitter[j] = StringManipulator.capitalize_first_character(splitter[j])
				except IndexError:
					pass

			methodBody[i] = StringManipulator.list_to_string(splitter)


		return methodBody
