from ..LangLibrary import Keywords


class StringManipulator:
	@staticmethod
	def capitalize_first_character(word):
		listToCapitalize = list(word)

		try:
			listToCapitalize[0] = listToCapitalize[0].upper()
		except IndexError:
			pass

		return ''.join(listToCapitalize)

	@staticmethod
	def clean_string(toClean):
		# Remove invisible characters
		toClean = StringManipulator.strip_double_space(toClean.replace('\t', ' ').replace('\n', ' '))

		# Strip trailing and leading spaces, then strip inline comments
		return StringManipulator.strip_inline_comment(toClean.strip())

	@staticmethod
	def check_sentence_split(sentence, splitter, indexCheck):

		"""
		Remove doubles spaces,
		then add spaces between the splitter,
		then strip leading and trailing spaces
		"""
		sentence = StringManipulator.strip_double_space(sentence).replace(
				splitter, ' ' + splitter + ' ').strip()

		try:
			# Try to match the index against splitter
			return sentence.split()[indexCheck] == splitter
		except IndexError:
			# If it fails, then there is no first index, and the list is empty
			return False

	@staticmethod
	def collapse_indices(textLine):
		if (StringManipulator.list_element_in_string(textLine, Keywords.method_indices)) or (
				StringManipulator.list_element_in_string(textLine, Keywords.object_array_indices)
		):
			collapseList = StringManipulator.split_preserve_spaces(textLine)

			for i in range(len(textLine)):
				if ("(" in collapseList[i]) or ("[" in collapseList[i]):

					try:

						if (StringManipulator.list_element_in_string(
								collapseList[i - 2],
								Keywords.pseudocode_meta
							)
						) is False:
							collapseList[i - 1] = ""
					except IndexError:
						# TODO This is a bug with multiline hubbub that I want to fix EVENTUALLY
						pass



					try:
						collapseList[i + 1] = ""
					except IndexError:
						# This error is a normal
						pass

				elif (")" in collapseList[i]) or ("]" in collapseList[i]):
					collapseList[i - 1] = ""

			textLine = StringManipulator.clean_string(''.join(collapseList))

		return textLine


	@staticmethod
	def exec_conversion_table(conversionTable, toConvert, fuzzyMatch=False):
		# Get keys of symbol conversion conversionTable
		keys = list(conversionTable.keys())
		# Get the values of the symbol conversion conversionTable
		values = list(conversionTable.values())

		"""
		Convert the symbols
		"""
		# If whole-word matching is off
		if fuzzyMatch:
			for i in range(len(conversionTable)):
				toConvert = toConvert.replace(keys[i], values[i])
		# If whole-word matching is on
		else:
			listToConvert = toConvert.split()

			for i in range(len(keys)):
				blockSize = len(keys[i].split())
				for j in range(len(listToConvert)):
					if keys[i].lower() == StringManipulator.list_to_string(
							listToConvert[j:j + blockSize]).lower():
						listToConvert[j:j + blockSize] = values[i].split()

			toConvert = StringManipulator.list_to_string(listToConvert)

		return toConvert

	@staticmethod
	def is_comment_line(sentence):
		return StringManipulator.check_sentence_split(sentence, '//', 0)

	@staticmethod
	def is_comment_multiline(sentence):
		return StringManipulator.check_sentence_split(sentence, '/*', 0)

	@staticmethod
	def is_comment_multiline_end(sentence):
		return StringManipulator.check_sentence_split(sentence, '*/', -1)

	@staticmethod
	def list_element_in_string(string, elementList, fuzzyMatch=False):
		return StringManipulator.first_occurrence_of_list_element_in_string(
				string, elementList, fuzzyMatch) is not None

	@staticmethod
	def first_occurrence_of_list_element_in_string(string, elementList, fuzzyMatch=False):
		try:
			if fuzzyMatch:
				if isinstance(string, str):
					return next(element for element in elementList if element.lower() in string.lower())
				else:
					for partOf in string:
						return next(element for element in elementList if element.lower() in partOf.lower())
			else:
				if isinstance(string, str):
					return next(element for element in elementList if element.lower() in string.lower().split())
				else:
					for partOf in string:
						return next(element for element in elementList if element.lower() in partOf.lower().split())
		except StopIteration:
			return None

	@staticmethod
	def list_to_string(listToCast):
		return StringManipulator.strip_double_space(''.join(element + ' ' for element in listToCast))

	@staticmethod
	def remove_words(sentence, naughtyWords, fuzzyMatch=False):
		return StringManipulator.exec_conversion_table({word: '' for word in naughtyWords}, sentence, fuzzyMatch)

	@staticmethod
	def split_preserve_spaces(stringToSplit):
		listSplitted = stringToSplit.split()

		for i in range(1, ((2 * len(stringToSplit)) - 1), 2):
			listSplitted.insert(i, " ")

		return listSplitted

	@staticmethod
	def strip_double_space(toStrip):
		# If there is a double space, remove it
		if '  ' in toStrip:
			toStrip = StringManipulator.strip_double_space(toStrip.replace('  ', ' '))

		# Recurse to check for more double spaces
		return toStrip.strip()

	@staticmethod
	def strip_inline_comment(toClean):
		# Make sure both '/*' and '*/' are in the same line
		if all(indicator in toClean for indicator in Keywords.comment_inline):
			# Ensure splitting will be compatible (add spaces in-between the characters)
			for indicator in Keywords.comment_inline:
				toClean = toClean.replace(indicator, ' ' + indicator + ' ')

			# Create variables for operation
			listToPurge = toClean.split()
			insideComment = False

			"""
			Begin conversion
			"""
			for i in range(len(listToPurge)):
				# If the beginning of a comment is found, set the iteration to remove each string
				if listToPurge[i] == '/*':
					insideComment = True

				# If set to remove each string,
				if insideComment:
					"""
					If the string is the ending-point of the comment, 
					disable removal of the next string, but allow the ending-point to be
					removed.
					"""
					if listToPurge[i] == '*/':
						insideComment = False
					# Remove the string at the current index.
					listToPurge[i] = ''

			# Convert the list back into a string
			toClean = StringManipulator.list_to_string(listToPurge)

		# Return the result of the string with all inline comments stripped out
		return toClean.strip()


