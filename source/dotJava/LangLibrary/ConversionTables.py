class_header_generics = {
		'<': ' < ',
		'>': ' > ',
		',': ' , '
}

keywords = {
		'break ;': 'BREAK;',
		'case': 'CASE',
		'continue ;': 'CONTINUE loop with next iteration;',
		'catch': 'CATCH',
		'default:': 'DEFAULT:',
		'do': 'DO:',
		'else': 'ELSE:',
		'false': 'False',
		'for': 'FOR ',
		'if': 'IF',
		'ELSE: IF': 'ELSE IF',
		'null': 'Null',
		'== null': 'is Null',
		'<> null': 'is not Null',
		'new': 'NEW',
		'return': 'RETURN',
		'super': 'SUPER',
		"super´s": "SUPER´s",
		'switch': 'SWITCH ',
		"this": "THIS",
		"THIS´s": "THIS",
		'throw': 'THROW',
		'true': 'True',
		'try': 'TRY:',
		'while': 'WHILE '
}

post_corrections = {
		'< >': ' <> ',
		' - >': ' ->',
		'NOT =': '!=',
		' ´s': '´s',
		' ;': ';'
}

syntax_splitter_compatible = {
		'<': ' < ',
		'>': ' > ',
		',': ' , ',
        '&&':'AND',
        '||':'OR',
		'(': ' ( ',
		')': ' ) ',
		'[': ' [ ',
		']': ' ] ',
		'!': '(NOT) ',
		';': ' ;',
		'++': ' INCREMENT ',
		'--': ' DECREMENT ',
}

symbols = {
		'=': ':=',
		'==': '=',
		'!=': '<>',
		'<=': '≤',
		'>=': '≥',
}

varargs_correction = {
		' ´s ´s ´s': '[]',
}
