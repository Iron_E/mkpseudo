class_header_indicators = ['class', 'implements']

class_header_anti_indicators = ['(', ')', ';', '[', ']', 'enum', 'interface']

class_headers_oop = ['extends', 'implements']

class_types = ['class', 'interface', 'enum']

control_flow = ['else if', 'if', 'else', 'while', 'do', 'switch', 'try', 'catch', 'for']

control_flow_pseudocode = ['ELSE IF', 'IF', 'ELSE:', 'WHILE', 'DO:', 'SWITCH', 'TRY:', 'CATCH', 'FOR']

comment_inline = ['/*', '*/']

comment_line_entire = ['//', '/*']

dot_operator = '.'

lambda_common = [
		'::', '.stream(', '.map', '.forEach(',
		' -> ', 'Collector.toCollection(', '.reduce(', '.filter('
		'.comparing'
]

method_header_oop = ['super', 'extends', 'throws']

method_indices = ['(', ')']

modifiers = [
		'public', 'private', 'protected', 'transient',
		'static', 'final', 'abstract', 'default',
		'throws'
]

object_array_indices = ['[', ']']

object_declare = ['new', 'extends', ',', '[]{']

object_declare_array_inline = ['[]{', '};', '},']

object_generics_indices = ['<', '>']

oop = ['return', 'this', 'super', 'throw', 'new', 'extends']

pseudocode_meta = [
		'CASE',
		'CATCH',
		'ELSE:',
		'FOR',
		'IF',
		'NEW',
		'RETURN',
		'SUPER',
		"SUPER´s",
        'SWITCH',
        "THIS",
		'THROW',
		'WHILE '
]

semicolon = ';'

symbols_equality = ['=', '==', '!=', '<', '>', '<=', '>=']

types_common = [
		'String', 'Integer', 'int', 'boolean',
		'void', 'float', 'double', 'char',
		'byte', 'short', 'long', 'Iterable',
		'null'
]

types_generic = [
		'E', 'K', 'T', 'V',
		'E[]', 'K[]', 'T[]', 'V[]'
]

types_generic_array = [
		'E[]', 'K[]', 'T[]', 'V[]'
]