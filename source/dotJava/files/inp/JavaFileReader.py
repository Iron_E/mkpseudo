import sys

from ...LangLibrary import ConversionTables, Keywords
from ..out.MarkdownFileWriter import MarkdownFileWriter
from ...util.StringFormatter import StringFormatter
from ...util.StringManipulator import StringManipulator


class JavaFileReader:
	"""
	FIELDS
	"""

	__braceEndIndexes = []
	__braceOpenIndexes = []
	__currentLine = ""
	__syntaxStack = ['']

	"""
	GETTERS
	"""

	@classmethod
	def get_brace_end_indexes(cls):
		return cls.__braceEndIndexes

	@classmethod
	def get_brace_open_indexes(cls):
		return cls.__braceOpenIndexes

	@classmethod
	def get_current_line(cls):
		return cls.__currentLine

	@classmethod
	def get_syntax_stack(cls):
		return cls.__syntaxStack

	"""
	INIT
	"""

	@classmethod
	def __init(cls):
		with open(sys.argv[1], 'r') as javaFile:
			for line in iter(javaFile.readline, ''):
				if '{' in line:
					cls.__braceOpenIndexes.append(javaFile.tell())
				if '}' in line:
					if StringManipulator.list_element_in_string(line,
					                                            Keywords.object_declare_array_inline,
					                                            True):
						cls.__braceOpenIndexes.pop()
					else:
						cls.__braceEndIndexes.append(javaFile.tell())

	"""
	SETTERS
	"""
	@classmethod
	def set_current_line(cls, nextLine):
		cls.__currentLine = StringManipulator.clean_string(nextLine)

	"""
	OPERATORS
	"""

	@classmethod
	def read_passed_file(cls):
		with open(sys.argv[1], 'r') as javaFile:

			# Init the class variables
			cls.__init()

			try:
				fileIterator = iter(javaFile.readline, '')

				def handled_comment_line():
					if StringManipulator.is_comment_line(cls.__currentLine):
						# Just pass if it is a comment
						return True

					# If not, check for the beginning of multiline comment
					elif StringManipulator.is_comment_multiline(cls.__currentLine):
						# This block skips the multiline comment
						while StringManipulator.is_comment_multiline_end(cls.__currentLine) is False:
							# Get the next  line (for testing against the while loop)
							cls.set_current_line(next(fileIterator))
						return True
					else:
						return False
				
				for iteratorLine in fileIterator:
					"""
					Clean the next line and assign it
					Cleaning includes stripping inline comments
					"""
					cls.set_current_line(iteratorLine)

					# Check for comment lines
					if handled_comment_line():
						pass

					# The `else` is executed if and only if the current line is not a comment
					else:

						# Convert lines given that it is not a comment
						cls.set_current_line(StringFormatter.format_to_pseudocode(cls.__currentLine))

						# Check for class header
						if cls.current_line_is_class_header():
							MarkdownFileWriter.write_class_header(cls.__currentLine)
						# If no class header, check for method indices, '(' or ')'
						elif StringManipulator.list_element_in_string(
								cls.__currentLine, Keywords.method_indices, True):

							# Assure no list indicators are present
							if cls.current_line_contains_array_instantiation() is False:
								# Assure no method-body indicators are in the current line
								if cls.current_line_is_method_body() is False:
									completeMethodSignature = ""

									# If the method signature is one line
									if all(indice in cls.__currentLine for indice in Keywords.method_indices):
										completeMethodSignature = cls.__currentLine
										cls.set_current_line(next(fileIterator))

									# If the method signature is multi-line
									else:
										while cls.__currentLine != "{":
											completeMethodSignature += ' ' + cls.__currentLine
											from time import sleep
											sleep(.04)
											cls.set_current_line(next(fileIterator))

									completeMethodSignature = completeMethodSignature.strip()

									MarkdownFileWriter.write_method_header(completeMethodSignature)

									"""
									Begin writing of method body
									"""
									# Advance past the opening bracket of the method body
									cls.set_current_line(next(fileIterator))

									# Add the opening bracket to the syntax stack
									totalOpenBrackets = 1
									totalEndBrackets = 0

									# Create list to store each line of the method body
									methodBody = ['{']

									# Loop until the ending bracket is found
									while totalOpenBrackets != totalEndBrackets:
										if handled_comment_line() is False:
											# Format to pseudocode
											cls.set_current_line(
													StringFormatter.format_to_pseudocode(
															cls.__currentLine
													)
											)
											# Detect open bracket
											if '{' in cls.__currentLine:
												totalOpenBrackets += 1
											# Detect closed bracket
											if '}' in cls.__currentLine:
												totalEndBrackets += 1
											# Append the current line because it is not a comment
											methodBody.append(cls.__currentLine)
										# Advance the current line
										cls.set_current_line(next(fileIterator))

									methodBody = StringFormatter.prepare_method_body(methodBody[1:-1])

									MarkdownFileWriter.write_method_body(methodBody)
			except StopIteration:
				pass

	"""
	currentLine tests
	"""
	@classmethod
	def current_line_contains_array_instantiation(cls):
		return (StringManipulator.list_element_in_string(
				cls.__currentLine, Keywords.semicolon, True) or
		        StringManipulator.list_element_in_string(
				        cls.__currentLine, Keywords.object_declare_array_inline, True))

	@classmethod
	def current_line_is_class_header(cls):
		return (StringManipulator.list_element_in_string(
				cls.__currentLine, Keywords.class_header_indicators)) and (
				       StringManipulator.list_element_in_string(
						       cls.__currentLine, Keywords.class_header_anti_indicators, True) is False)

	@classmethod
	def current_line_is_method_body(cls):
		return (StringManipulator.list_element_in_string(
				# Control flow
				cls.__currentLine.lower().split(), Keywords.control_flow) or
		        StringManipulator.list_element_in_string(
				        # Difference of oop keywords and those found in method headers
				        cls.__currentLine.lower().split(),
				        list(set(Keywords.oop) - set(Keywords.method_header_oop))
		        ) or StringManipulator.list_element_in_string(
						# Lambda keywords
						cls.__currentLine, Keywords.lambda_common, True
				) or StringManipulator.list_element_in_string(
						# Equality and assignment values
						cls.__currentLine,
						list(ConversionTables.symbols.keys())
				) or cls.__currentLine.count('(') > 1)
