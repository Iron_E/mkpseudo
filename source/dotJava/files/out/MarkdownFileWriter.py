import sys

from ...LangLibrary import ConversionTables, Keywords
from ...util.StringFormatter import StringFormatter
from ...util.StringManipulator import StringManipulator


class MarkdownFileWriter:
	__currentClassName = ""

	@classmethod
	def __determine_method_name(cls, encounteredName):
		if StringFormatter.class_to_type(encounteredName).split()[0] == \
				cls.__currentClassName.split()[0]:
			return "Constructor"
		else:
			return StringManipulator.capitalize_first_character(encounteredName)

	@staticmethod
	def __trim_class_header_oop(header):
		syntaxStack = ['']

		for word in header.split():
			# Allow for detection if the iterator is in the middle of a generic declaration
			if word == '<':
				header.append('<')
			# Dismiss generic declaration
			elif word == '>':
				header.pop()

			# If the word is a keyword, and we are NOT in a generic declaration
			if (word in Keywords.class_headers_oop) and (
					(syntaxStack[-1] == '<') is False):
				# Split the class name at the current word and leave the loop
				del syntaxStack
				header = header.split(word)[0]
				break

		return header

	@staticmethod
	def __useless_oop_info(potentialClass):
		return list(potentialClass)[-1] == 's'

	@classmethod
	def write_class_header(cls, textLine):
		with open(sys.argv[2], 'a') as mdFile:
			# Remove object-oriented information
			textLine = cls.__trim_class_header_oop(
					# Remove class-type information ("enum", "class", "interface", etc)
					StringManipulator.remove_words(
							textLine, Keywords.class_types
					)
			)

			# Format the currentClass to the pseudocode type format
			cls.__currentClassName = StringFormatter.class_to_type(textLine)

			mdFile.write(f"\n<center><h2>{cls.__currentClassName}.java</h2></center>\n")

	@classmethod
	def write_method_header(cls, textLine):
		with open(sys.argv[2], 'a') as mdFile:
			textLine = StringManipulator.exec_conversion_table(
					ConversionTables.varargs_correction,
					(textLine.replace('(', ' ( ').replace(')', ' ) ')),
					True
			).split()

			# Remove local generic types (passed _into_ a method through parameters)
			if list(textLine[0])[0] == "<":
				textLine = textLine[1:]

			for i in range(len(textLine)):
				if textLine[i] == '(':
					# Format the method name (or set it to "Constructor")
					methodName = cls.__determine_method_name(textLine[i - 1])

					# Write the method name in MD format
					mdFile.write(f'\n### {methodName}\n')

					# If the name is not Constructor, then it returns a type
					if methodName != "Constructor":
						# Write the "returning" field
						mdFile.write("\n* Returning ")

						listToWrite = []

						# Build the list of types being returned
						for j in range(i - 1):
							# Strip out useless OOP information
							if cls.__useless_oop_info(textLine[j]) is False:
								if "<" in textLine[j] and ">" not in textLine[j]:
									k = j
									while ">" not in textLine[k - 1]:
										listToWrite.append(textLine[k])
										k += 1

									j += 1
								else:
									listToWrite.append(textLine[j])

						# Get rid of commands and convert to "returning" format
						if len(listToWrite) != 0:

							# print(textLine)
							listToWrite = StringFormatter.class_to_returning_type(
									listToWrite).replace(', ', '')
						else:
							listToWrite = ""

						# Write the list
						mdFile.write(listToWrite)
				try:
					# Select the first entry AFTER the parentheses
					if textLine[i - 1] == '(':
						listToWrite = []
						for j in range(i, len(textLine) - 1):
							if ((cls.__useless_oop_info(textLine[j])) or (
									textLine[j] == ")"
							)) is False:
								# Append lines ONLY IF they are not useless information
								listToWrite.append(
										textLine[j]
								)

						# Assure list contains information
						if len(listToWrite) > 0:
							mdFile.write('\n* Input Variables:')
							# Update list
							listToWrite = [
									stringList.split() for stringList in StringManipulator.list_to_string(
											listToWrite).split(',')
							]

							# Write all lines as tabbed, returning entries
							for internalList in listToWrite:
								mdFile.write(
										StringFormatter.format_to_tabbed_list_entry(
												StringFormatter.class_to_returning_type(internalList)
										)
								)

				except IndexError:
					# Index errors at 0 are normal
					if i == 0:
						continue
					else:
						raise IndexError

	@classmethod
	def write_method_body(cls, methodBody):
		with open(sys.argv[2], 'a') as mdFile:
			if methodBody != []:
				tabsList = []

				variables = "\n* Variables:" + \
				            "\n\t* Placeholder"

				subroutines = "\n* Subroutines:" + \
				              "\n\t* Placeholder"

				syntaxStack = []

				def update_tabs(index):
					updatedTabs = '\t'

					try:
						if ("SWITCH" in syntaxStack) and (methodBody[index][:6] != "SWITCH") and ((
								methodBody[index][-1] != ":") or (
								methodBody[index].split()[0] in Keywords.control_flow_pseudocode)):
							tabRange = len(syntaxStack) + syntaxStack.count("SWITCH")
						else:
							tabRange = len(syntaxStack)
					except IndexError:
						tabRange = len(syntaxStack)

					for k in range(tabRange):
						updatedTabs += '\t'
					return updatedTabs

				methodBody = [line for line in methodBody if line != '{']

				"""
				Perform operations on method text
				"""

				for i in range(len(methodBody)):
					# Update the number of tabs to use
					tabs = update_tabs(i)
					# Detect and return (if any) control flow keywords
					controlFlow = StringManipulator.first_occurrence_of_list_element_in_string(
							methodBody[i], Keywords.control_flow_pseudocode)

					# Only execute if there is a control flow keyword present
					if controlFlow is not None:
						if "ELSE" not in methodBody[i]:
							syntaxStack.append(controlFlow.replace(':', ''))
						else:
							if "IF" in methodBody[i]:
								syntaxStack.append('ELSE IF')
							else:
								syntaxStack.append('ELSE')

						tabs = update_tabs(i)

						"""
						Rules for translating "for" loops
						"""

						if controlFlow.split()[-1] == 'IF':
							methodBody[i] = methodBody[i][:methodBody[i].index('(') - 1] + methodBody[i][methodBody[
								                                                                         i].index('('
							                                                                                      '') + 1:-2] + ":"
						elif (controlFlow == 'WHILE') or (controlFlow == 'SWITCH'):
							methodBody[i] = methodBody[i] + ":"

						elif controlFlow == 'CATCH':
							exception = methodBody[i].split()[2]

							if "Error" in exception:
								exception = exception[:-5]
							else:
								exception = exception[:-9]

							exception = StringFormatter.insert_spaces_in_exception_title(exception)

							methodBody[i] = f'CATCH "{exception}" Exceptions:'

						elif controlFlow == 'FOR':
							edit = methodBody[i].split()[2:]

							"""
							Basic integer iteration
							"""
							try:

								if edit[0] == 'int':
									initialization = ""
									upperRange = ""

									for j in range(len(edit)):
										if ";" not in edit[j]:
											initialization += edit[j] + ' '
										else:
											break

									for j in range(len(edit)):
										if ("<" in edit[j]) or (">" in edit[j]):
											while ';' not in edit[j + 1]:
												upperRange += edit[j + 1] + ' '
												j += 1
											break

									methodBody[i] = "FOR (" + initialization.strip() + ") TO " + upperRange.strip() + ":"

									"""
									ForEach iteration
									"""
								elif StringManipulator.list_element_in_string(':', edit):
									upperRange = ""
									syntaxStack[-1] = "FOREACH"
									separatorIndex = edit.index(':')

									identifier = edit[separatorIndex - 1]
									initialization = edit[separatorIndex - 2]

									edit[separatorIndex - 1] = ""

									for j in range(separatorIndex + 1, len(edit) - 1):
										upperRange += StringManipulator.capitalize_first_character(edit[j]) + ' '

									methodBody[i] = "FOREACH `" + \
													initialization.strip() + \
													"` IN " + upperRange.strip() + ', AS "' + \
													StringManipulator.capitalize_first_character(identifier.strip()) + '":'
							except IndexError:
								# TODO add support for multiline for loop initialization
								pass

					"""
					Detect the end of inner-
					"""

					try:
						if methodBody[i][0] == '}':
							if methodBody[i] == '}':
								methodBody[i] = 'END-' + syntaxStack.pop()
							else:
								methodBody[i] = methodBody[i][2:]
								syntaxStack.pop()
							tabs = update_tabs(i)
					except IndexError:
						pass

					if (len(tabs) > 0) and (controlFlow is not None):
						tabs = tabs[1:]

					# methodBody[i] = tabs + methodBody[i]
					tabsList.insert(i, tabs)
				del update_tabs

				mdFile.write(f'{variables}{subroutines}' +
				             '\n\n```pseudocode' +
				             '\nSTART')

				for line, tab in zip(methodBody, tabsList):
					mdFile.write(f"\n{tab}{StringManipulator.collapse_indices(line)}")

				mdFile.write('\nSTOP' +
				             '\n```\n')
			else:
				mdFile.write('\nEMPTY\n')
