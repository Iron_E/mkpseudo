from setuptools import setup, find_packages

setup(name='mkspeudo',
      version='1.1',
      url='https://gitlab.com/Iron_E/mkpseudo',
      license='CC BY-SA 4.0',
      author='Iron_E',
      description='Generate pseudocode from sourcecode of various programming languages.',
      packages=find_packages(),
      long_description=open("README.md").read())
